/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) Martin Koehler, 2015-2022
 * Copyright (C) Martin Koehler, Jonas Schulze, 2023
 */

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <assert.h>
#include <complex.h>

#ifdef ALLOCA_H_FOUND
#include <alloca.h>
#endif

#include <curl/curl.h>
#include <sqlite3.h>
#include <matio.h>
#include <archive.h>
#include <archive_entry.h>

#include "libufget.h"
#include "uf_internal.h"

static int __uf_matrix_conv_memsave = 0;
int uf_matrix_conv_memsave(int save)
{
    int old = __uf_matrix_conv_memsave;
    __uf_matrix_conv_memsave = save;
    return old;
}

// #define LIBC_SORT

#ifdef LIBC_SORT
#warning COORD -> CSR conversion does not work inplace
typedef struct _mat_entry_t {
    int64_t row;
    int64_t col;
    value_t val;
} mat_entry_t;

static int comp_csr(const void *_a, const void *_b)
{
    const mat_entry_t* a = (const mat_entry_t *) _a;
    const mat_entry_t* b = (const mat_entry_t *) _b;

    if ( a->row < b -> row ) {
        return -1;
    }
    if ( a->row > b->row ) {
        return 1;
    }
    if ( a-> row == b->row ) {
        if ( a->col < b->col) {
            return -1;
        } else if ( a->col > b->col ) {
            return 1;
        }
        return 0;
    }
    return 0;
}
#else
static int comp_csr2(size_t arow, size_t acol, size_t brow, size_t bcol)
{
    // printf("COMP: %ld %ld <-> %ld %ld\n", arow, acol, brow, bcol);
    if ( arow < brow ) {
        return -1;
    }
    if ( arow > brow ) {
        return 1;
    }
    if ( arow == brow ) {
        if ( acol < bcol) {
            return -1;
        } else if ( acol > bcol ) {
            return 1;
        }
        return 0;
    }
    return 0;
}


#define EXCH(i, j) \
        memcpy(r, rowptr + (i)*row_index_size, row_index_size); /* r = rowptr[i] */ \
        memcpy(c, colptr + (i)*col_index_size, col_index_size); /* c = colptr[i] */ \
        memcpy(v, values + (i)*valsize, valsize); /* v = values[i] */ \
        memcpy(rowptr + (i)*row_index_size, rowptr + (j)*row_index_size, row_index_size); /* rowptr[i] = rowptr[j] */ \
        memcpy(colptr + (i)*col_index_size, colptr + (j)*col_index_size, col_index_size); /* colptr[i] = colptr[j] */ \
        memcpy(values + (i)*valsize, values + (j)*valsize, valsize); /* values[i] = values[j] */ \
        memcpy(rowptr + (j)*row_index_size, r, row_index_size); /* rowptr[j] = r */ \
        memcpy(colptr + (j)*col_index_size, c, col_index_size); /* colptr[j] = c */ \
        memcpy(values + (j)*valsize, v, valsize); /* values[j] = v */

#define COMPEXCH(i, j) \
        if (comp_csr2(get_row(rowptr, i), get_col(colptr, i), get_row(rowptr, j), get_col(colptr, j)) == -1){ \
            EXCH(i, j); \
        }

typedef struct {
    size_t len;
    void *rptr;
    void *cptr;
    void *vptr;
} stack_node;

// Set during tests
LIBUFGET_EXPORT size_t uf_get_quicksort_thresh = 32;

#define STACK_SIZE  64
#define PUSH(n, rowptr, colptr, values) ((void) ((top->len  = (n)), (top->rptr = (rowptr)), (top->cptr = (colptr)), (top->vptr = (values)), ++top))
#define POP(n, rowptr, colptr, values)  ((void) (--top, (n = top->len), (rowptr = top->rptr)), (colptr = top->cptr), (values = top->vptr))
#define STACK_NOT_EMPTY (stack < top)


/*-----------------------------------------------------------------------------
 *  Quick Sort
 *-----------------------------------------------------------------------------*/
static void qs(
        size_t n
        , char* rowptr, uf_index_t row_type
        , char* colptr, uf_index_t col_type
        , char* values, size_t valsize
        ) {

    if (n <= uf_get_quicksort_thresh) return;

    stack_node stack[STACK_SIZE];
    stack_node* top = stack;
    size_t i, j, p;
    char* r = alloca(index_size(row_type));
    char* c = alloca(index_size(col_type));
    char* v = alloca(valsize);

    get_int_func get_row = get_index(row_type);
    get_int_func get_col = get_index(col_type);
    size_t row_index_size = index_size(row_type);
    size_t col_index_size = index_size(col_type);

    PUSH(0, NULL, NULL, NULL);
    while (STACK_NOT_EMPTY) {
        if (n <= uf_get_quicksort_thresh) {
            POP(n, rowptr, colptr, values);
            continue;
        }
        p = n - 1;
        if (n > 3) {
            EXCH(n/2, p-1);
            COMPEXCH(0, p-1);
            COMPEXCH(0, p);
            COMPEXCH(p-1, p);
        } else {
            EXCH(n/2, p);
        }
        for (i = 0, j = n - 1; ; i++, j--) {
            while (comp_csr2(get_row(rowptr, i),get_col(colptr, i),get_row(rowptr, p),get_col(colptr, p)) == -1)
                i++;
            while (comp_csr2(get_row(rowptr, p),get_col(colptr, p),get_row(rowptr, j),get_col(colptr, j)) == -1) {
                j--;
                if (j == i) break;
            }
            if (i >= j) break;
            EXCH(i, j);
        }
        EXCH(i, p);
        if ( i > n-i-1) {
            PUSH(i, rowptr, colptr, values);
            n = n-i-1;
            rowptr += (i+1)*row_index_size;
            colptr += (i+1)*col_index_size;
            values += (i+1)*valsize;
        } else {
            PUSH(n-i-1, rowptr + (i+1)*row_index_size, colptr + (i+1)*col_index_size, values + (i+1)*valsize);
            n = i;
        }
    }
}

/*-----------------------------------------------------------------------------
 *  Insertion Sort
 *-----------------------------------------------------------------------------*/
static void is(
        size_t n
        , char* rowptr, uf_index_t row_type
        , char* colptr, uf_index_t col_type
        , char* values, size_t val_size
        ) {
    size_t i, j;
    size_t r, c;
    char* v = alloca(val_size);

    get_int_func get_row = get_index(row_type);
    get_int_func get_col = get_index(col_type);
    set_int_func set_row = set_index(row_type);
    set_int_func set_col = set_index(col_type);
    size_t row_index_size = index_size(row_type);
    size_t col_index_size = index_size(col_type);

    for (i = 1; i < n; ++i) {
        r = get_row(rowptr, i);
        c = get_col(colptr, i);
        memcpy(v, values + i*val_size, val_size); // v = values[i];
        for (j = i; j > 0 && comp_csr2(r, c, get_row(rowptr, j-1), get_col(colptr, j-1)) == -1; --j) {
            memcpy(rowptr + j*row_index_size, rowptr + (j-1)*row_index_size, row_index_size); // rowptr[j] = rowptr[j-1];
            memcpy(colptr + j*col_index_size, colptr + (j-1)*col_index_size, col_index_size); // colptr[j] = colptr[j-1];
            memcpy(values + j*val_size, values + (j-1)*val_size, val_size); // values[j] = values[j-1];
        }
        set_row(rowptr, j, r);
        set_col(colptr, j, c);
        memcpy(values + j*val_size, v, val_size); // values[j] = v;
    }
}

/*-----------------------------------------------------------------------------
 *  Hybrid Sort
 *-----------------------------------------------------------------------------*/
static void sort(
        size_t n
        , char* rowptr, uf_index_t rowtype
        , char* colptr, uf_index_t coltype
        , char* values, size_t valsize
        ) {
    qs(n, rowptr, rowtype, colptr, coltype, values, valsize);
    is(n, rowptr, rowtype, colptr, coltype, values, valsize);
}

#endif

typedef struct _vmat_entry_t {
    size_t col;
    value_t val;
} vmat_entry_t;

static int comp_row(const void *_a, const void *_b)
{
    const vmat_entry_t* a = (const vmat_entry_t *) _a;
    const vmat_entry_t* b = (const vmat_entry_t *) _b;

    if ( a->col < b -> col ) {
        return -1;
    }
    if ( a->col > b->col ) {
        return 1;
    }
    return 0;
}


/* #define PRINT(NAME, INTTYPE) void NAME ( size_t N, INTTYPE *rowptr, INTTYPE *colptr) {\
    size_t i; \
    for (i = 0; i < N; i++) {\
    printf("%6" PRId64 "\t %6"PRId64"\n", (int64_t) rowptr[i], (int64_t) colptr[i]);\
    }\
    }

    PRINT(pr32, int32_t)
    PRINT(pr64, int64_t) */

static inline void copy_value(char* dst, size_t dst_id, char const* src, size_t src_id, size_t val_size)
{
    memcpy(dst + dst_id*val_size, src + src_id*val_size, val_size);
}

/*-----------------------------------------------------------------------------
 *  generic convert to CSR function
 *-----------------------------------------------------------------------------*/
int uf_matrix_coord_to_csr_extended(
        uf_field_t field
        , int64_t nrows, int64_t ncols, int64_t nnz
        , void **rowptr, uf_index_t rowtype
        , void **colptr, uf_index_t coltype
        , void **values, uf_precision_t valtype
        )
{
    if (!index_type_ok(rowtype, nnz)) {
        DPRINTF(3, "Row pointer type not large enough to store nnz.\n");
        return -1;
    }

    void *irowptr = *rowptr;
    void *icolptr = *colptr;

    /* Create setter functions and other metadata */
    size_t row_index_size = index_size(rowtype);
    size_t col_index_size = index_size(coltype);
    size_t val_size = value_size(field, valtype);
    set_int_func set_row = set_index(rowtype);
    set_int_func set_col = set_index(coltype);
    set_val_func set_val = set_value(field, valtype);
    get_int_func get_row = get_index(rowtype);
    get_int_func get_col = get_index(coltype);
    get_val_func get_val = get_value(field, valtype);

    if (!row_index_size || !set_row || !get_row) {
        DPRINTF(0, "Invalid row index type\n");
        return -1;
    }
    if (!col_index_size || !set_col || !get_col) {
        DPRINTF(0, "Invalid column index type\n");
        return -1;
    }
    if (!val_size || !set_val || !get_val) {
        DPRINTF(0, "Invalid combination of uf_field_t and uf_precision_t\n");
        return -1;
    }

    if ( __uf_matrix_conv_memsave == 0 ) {
        size_t *rowcount;
        size_t *orowptr, *ocolptr;
        size_t pos, j;
        size_t i;
        void *val, *oval;
        vmat_entry_t *row;


        rowcount = (size_t *) malloc(sizeof(size_t) * (size_t)(nrows));
        orowptr  = (size_t *) malloc(sizeof(size_t) * (size_t)(nrows+1));
        ocolptr  = (size_t *) malloc(sizeof(size_t) * (size_t)(nnz));
        row      = (vmat_entry_t *) malloc(sizeof(vmat_entry_t) * (size_t) (ncols));

        oval = malloc(val_size * (size_t) (nnz*2));
        val = *values;

        // count entries in rows
        for (i = 0; i < nrows; i++)  rowcount[i] = 0;
        for (i = 0; i < nnz; i++) rowcount[get_row(irowptr,i)]++;

        // fill rowptr
        orowptr[0] = 0;
        for ( i = 0; i < nrows; i++){
            orowptr[i+1] = orowptr[i] + rowcount[i];
            rowcount[i] = orowptr[i];
        }
        // fill values
        for (i = 0; i < nnz; i++) {
            pos = rowcount[get_row(irowptr,i)];
            ocolptr[pos] = get_col(icolptr,i);
            copy_value(oval, pos, val, i, val_size);
            rowcount[get_row(irowptr,i)]++;
        }

        for (i = 0; i < nrows; i++) {
            pos = 0;
            for (j = orowptr[i]; j < orowptr[i+1]; j++) {
                row[pos].col = ocolptr[j];
                row[pos].val = get_val(oval, (size_t) j);
                pos++;
            }
            qsort(row, (size_t) pos, sizeof(vmat_entry_t), comp_row);
            pos = 0 ;
            for (j = orowptr[i]; j < orowptr[i+1]; j++) {
                set_col(icolptr, (size_t) j, row[pos].col);
                set_val(val, (size_t) j, row[pos].val);
                pos++;
            }
        }
        irowptr = realloc(irowptr, row_index_size * (size_t) (nrows+1));
        for (i = 0; i < nrows+1; i++) {
            set_row(irowptr, i, orowptr[i]) ;
        }

        *rowptr = irowptr;
        FREE(rowcount);
        FREE(orowptr);
        FREE(ocolptr);
        FREE(oval);
        FREE(row);
        return 0;

    } else { /* Save memory but slower   */
        void* val;
        size_t i = 0, irow;

#if defined(LIBC_SORT)
        mat_entry_t *mat = NULL;
        mat = (mat_entry_t *) malloc(sizeof(mat_entry_t) * (size_t)(nnz));
        val = *values;
        for (i = 0; i < nnz; i++) {
            mat[i].row = get_row(irowptr, (size_t) i);
            mat[i].col = get_col(icolptr, (size_t) i);
            mat[i].val = get_val(val, (size_t) i);
        }
        qsort(mat, (size_t) nnz, sizeof(mat_entry_t), comp_csr);
        for (i = 0; i < nnz; i++) {
            set_row(irowptr, (size_t) i, mat[i].row);
            set_col(icolptr, (size_t) i, mat[i].col);
            set_val(val, (size_t) i, mat[i].val);
        }
            /* if (intsize == 4 ) {
               pr32(nnz, irowptr, icolptr);
               } else {
               pr64(nnz, irowptr, icolptr);
               } */
        FREE(mat);
#else
        val = *values;

        sort((size_t) nnz, irowptr, rowtype, icolptr, coltype, val, val_size);
#endif

        /* Sum up  */
        void * irowptr2 = malloc(row_index_size * (size_t) (nrows+1));
        if (nrows+1 >= nnz) {
            irowptr = realloc(irowptr, row_index_size * (size_t) (nrows+1));
        }
        irow = 0;
        i = 0;
        for (irow = 0; irow < nrows; irow++) {
            while ( i < nnz && get_row(irowptr, (size_t) i) == irow ) {
                i++;
            }
            set_row(irowptr2, irow+1, i);
        }
        if ( i!= nnz ) {
            DPRINTF(3, "i!=nnz: %ld - %ld\n", i, nnz);
            FREE(irowptr2);
            return -1;
        }
        // assert(i==nnz);
        set_row(irowptr2, 0, 0);
        set_row(irowptr2, (size_t) nrows, (size_t) nnz);
        irowptr = realloc(irowptr, row_index_size * (size_t) (nrows+1));
        memcpy(irowptr, irowptr2, row_index_size * (size_t) (nrows+1));
        FREE(irowptr2);
        *rowptr = irowptr;

        return 0;
    }
    return 0;
}

/*-----------------------------------------------------------------------------
 *  Interface COORD -> CSR  with 4 byte integers
 *-----------------------------------------------------------------------------*/
int uf_matrix_coord_to_csr_int32(uf_field_t field, int32_t nrows, int32_t ncols, int32_t nnz, int32_t **rowptr, int32_t **colptr, void **values)
{
    return uf_matrix_coord_to_csr_extended(
            field
            , nrows, ncols, nnz
            , (void **) rowptr, UF_I32
            , (void **) colptr, UF_I32
            , (void **) values, UF_F64
            );
}

/*-----------------------------------------------------------------------------
 *  Interface COORD -> CSR  with 8 byte integers
 *-----------------------------------------------------------------------------*/
int uf_matrix_coord_to_csr_int64(uf_field_t field, int64_t nrows, int64_t ncols, int64_t nnz, int64_t **rowptr, int64_t **colptr, void **values)
{
    return uf_matrix_coord_to_csr_extended(
            field
            , nrows, ncols, nnz
            , (void **) rowptr, UF_I64
            , (void **) colptr, UF_I64
            , (void **) values, UF_F64
    );
}

/*-----------------------------------------------------------------------------
 *  Interface COORD -> CSC  with 4 byte integers
 *-----------------------------------------------------------------------------*/
int uf_matrix_coord_to_csc_int32(uf_field_t field, int32_t nrows, int32_t ncols, int32_t nnz, int32_t **rowptr, int32_t **colptr, void **values)
{
    return uf_matrix_coord_to_csr_extended(
            field
            , nrows, ncols, nnz
            , (void **) colptr, UF_I32
            , (void **) rowptr, UF_I32
            , (void **) values, UF_F64
            );
}

/*-----------------------------------------------------------------------------
 *  Interface COORD -> CSC  with 8 byte integers
 *-----------------------------------------------------------------------------*/
int uf_matrix_coord_to_csc_int64(uf_field_t field, int64_t nrows, int64_t ncols, int64_t nnz, int64_t **rowptr, int64_t **colptr, void **values)
{
    return uf_matrix_coord_to_csr_extended(
            field
            , nrows, ncols, nnz
            , (void **) colptr, UF_I64
            , (void **) rowptr, UF_I64
            , (void **) values, UF_F64
    );
}

