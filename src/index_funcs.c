/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) Martin Koehler, Jonas Schulze, 2023
 */

#include "libufget.h"
#include "uf_internal.h"

#define SET_INDEX(type) \
static void set_index_##type(void *array, size_t pos, size_t value) \
{ \
    type* iarray = (type*) array; \
    iarray[pos] = (type) value; \
}

SET_INDEX(int8_t)
SET_INDEX(int16_t)
SET_INDEX(int32_t)
SET_INDEX(int64_t)
SET_INDEX(uint8_t)
SET_INDEX(uint16_t)
SET_INDEX(uint32_t)
SET_INDEX(uint64_t)

#undef SET_INDEX

set_int_func set_index(uf_index_t p)
{
    switch (p) {
        case UF_I8: return set_index_int8_t;
        case UF_I16: return set_index_int16_t;
        case UF_I32: return set_index_int32_t;
        case UF_I64: return set_index_int64_t;
        case UF_U8: return set_index_uint8_t;
        case UF_U16: return set_index_uint16_t;
        case UF_U32: return set_index_uint32_t;
        case UF_U64: return set_index_uint64_t;
        default:
            DPRINTF(0, "Unknown index precision: %d\n", p);
            return NULL;
    }
}

#define GET_INDEX(type) \
static size_t get_index_##type(void *array, size_t pos) { \
    type* iarray = (type*) array; \
    size_t ret = (size_t) iarray[pos]; \
    return ret; \
}

GET_INDEX(int8_t)
GET_INDEX(int16_t)
GET_INDEX(int32_t)
GET_INDEX(int64_t)
GET_INDEX(uint8_t)
GET_INDEX(uint16_t)
GET_INDEX(uint32_t)
GET_INDEX(uint64_t)

#undef GET_INDEX

get_int_func get_index(uf_index_t p)
{
    switch (p) {
        case UF_I8: return get_index_int8_t;
        case UF_I16: return get_index_int16_t;
        case UF_I32: return get_index_int32_t;
        case UF_I64: return get_index_int64_t;
        case UF_U8: return get_index_uint8_t;
        case UF_U16: return get_index_uint16_t;
        case UF_U32: return get_index_uint32_t;
        case UF_U64: return get_index_uint64_t;
        default:
            DPRINTF(0, "Unknown index precision: %d\n", p);
            return NULL;
    }
}

int index_type_ok(uf_index_t p, int64_t max_value)
{
    switch (p) {
        case UF_I8: return max_value <= INT8_MAX;
        case UF_I16: return max_value <= INT16_MAX;
        case UF_I32: return max_value <= INT32_MAX;
        case UF_I64: return 1;
        case UF_U8: return max_value <= UINT8_MAX;
        case UF_U16: return max_value <= UINT16_MAX;
        case UF_U32: return max_value <= UINT32_MAX;
        case UF_U64: return 1;
        default:
            DPRINTF(0, "Unknown index precision: %d\n", p);
            return 0;
    }
}

size_t index_size(uf_index_t p)
{
    switch (p) {
        case UF_I8: return sizeof(int8_t);
        case UF_I16: return sizeof(int16_t);
        case UF_I32: return sizeof(int32_t);
        case UF_I64: return sizeof(int64_t);
        case UF_U8: return sizeof(uint8_t);
        case UF_U16: return sizeof(uint16_t);
        case UF_U32: return sizeof(uint32_t);
        case UF_U64: return sizeof(uint64_t);
        default:
            DPRINTF(0, "Unknown index precision: %d\n", p);
            return 0;
    }
}
