/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) Martin Koehler, 2015-2022
 * Copyright (C) Martin Koehler, Jonas Schulze, 2023
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>

#include "libufget.h"
#include "uf_internal.h"
#include "io/io.h"


void libufget_fprintf(FILE *stream, char const *msg, ...)
{
    const char* fmt = "[libufget (pid = %d)] %s";
    pid_t pid = getpid();
    size_t bufsize = (size_t) snprintf(NULL, 0, fmt, pid, msg);
    char *local_msg = calloc(bufsize+1, sizeof(char));
    sprintf(local_msg, fmt, pid, msg);
    va_list args;
    va_start(args, msg);
    vfprintf(stream, local_msg, args);
    va_end(args);
    FREE(local_msg);
}

