 /*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) Martin Koehler, 2015-2022
 * Copyright (C) Martin Koehler, Jonas Schulze, 2023
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sqlite3.h>

#include "libufget.h"
#include "uf_internal.h"

int uf_table_drop(sqlite3 *db, const char * table) {
    char drop_table[256];
    snprintf(drop_table, 256, "DROP TABLE IF EXISTS %s;", table);
    int rc  = sql_execute(db, drop_table);
    if ( rc == SQLITE_OK)
        return 0;
    return -1;
}

int uf_table_meta(sqlite3 *db) {
    int rc = sql_execute(db, "CREATE TABLE meta (key TEXT UNIQUE, value TEXT);");
    if ( rc != SQLITE_OK ) {
        DPRINTF(1,"Failed to create meta table.\n");
        return -1;
    }
    return 0;
}


int uf_table_matrices(sqlite3 *db)
{
    int rc;

    rc = sql_execute(db, "CREATE TABLE IF NOT EXISTS matrices( id INTEGER PRIMARY KEY, group_name TEXT, name TEXT, nrows INTEGER, ncols INTEGER, nnz INTEGER, nzeros INTEGER, "
                         "pattern_symmetry REAL, numerical_symmetry REAL, isBinary INTEGER, isReal INTEGER, isComplex INTEGER, nnzdiag INTEGER, posdef INTEGER, amd_lnz INTEGER, amd_flops INTEGER,"
                         "amd_vnz INTEGER, amd_rnz INTEGER, nblocks INTEGER, sprank INTEGER, RBtype TEXT, cholcand INTEGER, ncc INTEGER, isND INTEGER, isGraph INTEGER, "
                         "lowerbandwidth INTEGER, upperbandwidth INTEGER, rcm_lowerbandwidth INTEGER, rcm_upperbandwidth INTEGER, xmin_real REAL, xmin_imag REAL, xmax_real REAL, xmax_imag REAL);");

    if ( rc != SQLITE_OK ) {
        DPRINTF(1,"Failed to create matrices table.\n");
        return -1;
    }
    return 0;
}

