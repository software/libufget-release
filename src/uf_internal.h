/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) Martin Koehler, 2015-2022
 * Copyright (C) Martin Koehler, Jonas Schulze, 2023
 */


#ifndef UF_INTERNAL_H

#define UF_INTERNAL_H

#define UF_INDEX_FILE "files/ss_index.mat"
#define UF_INDEX_URL "http://sparse-files.engr.tamu.edu"
#define UF_CONTAINER_STRUCT "ss_index"
#define UF_SQLITE_TABLE_VERSION 4

#define UFGET_COPYRIGHT_STRING "Copyright (C) 2015-2023 Martin Koehler, Jonas Schulze"


#include "libufget_exports.h"
#include <sqlite3.h>
#include "uf_verbose.h"
#include <sys/stat.h>
#include <sys/types.h>
#include <complex.h>

enum file_lock_flags {
    FILE_LOCK_NOWAIT,
    FILE_LOCK_WAIT,
};

/*! \struct _uf_file_lock_t
 *  \brief Brief struct description
 *
 *  Detailed description
 */
typedef struct _uf_file_lock_t {
    int fd;
    char *filename;
    char *desc;
    int use_inotify;
} uf_file_lock_t;


struct _uf_collection_t {
    sqlite3 *db;
    char *baseurl;
    char *cache_dir;
    time_t stamp;
    unsigned long flags;
    mode_t dir_mode;
    mode_t file_mode;
    uf_file_lock_t *lock;

};
// typedef struct _uf_collection_t uf_collection_t;

struct _uf_query_t {
    struct _uf_collection_t const* col;
    sqlite3_stmt *stmt;
    int done;
    char* where_clause;
    char *isql;
};
// typedef struct _uf_query_t uf_query_t;

struct _uf_matrix_pimpl {
    uf_collection_t const* col;
};
// typedef struct _uf_matrix_pimpl uf_matrix_pimpl;

LIBUFGET_NO_EXPORT uf_matrix_t * uf_matrix_new(void);
LIBUFGET_NO_EXPORT void uf_matrix_from_stmt(uf_matrix_t *matrix, sqlite3_stmt *res);
LIBUFGET_NO_EXPORT char * uf_matrix_filename(uf_matrix_t const* mat);
LIBUFGET_NO_EXPORT void libufget_fprintf(FILE *stream, char const *msg, ...);

LIBUFGET_NO_EXPORT long sql_get_long(sqlite3 * db, const char * sql, ...);
LIBUFGET_NO_EXPORT int sql_table_exists(sqlite3 * db, const char *table_name);
LIBUFGET_NO_EXPORT int sql_get_table(sqlite3 *db, char ***resultp, int *nrow, int *ncol, const char *sql, ...);
LIBUFGET_NO_EXPORT int sql_execute(sqlite3 *db, const char* sql, ...);

#define UF_COLLECTION_LOCK_OK       0
#define UF_COLLECTION_LOCK_FAILED   1
#define UF_COLLECTION_LOCK_ERROR    2

LIBUFGET_NO_EXPORT uf_file_lock_t * uf_file_lock_init(const char * filename, const char * desc);
LIBUFGET_NO_EXPORT void uf_file_lock_free(uf_file_lock_t * lock);
LIBUFGET_NO_EXPORT void uf_file_unlock(uf_file_lock_t * lock);
LIBUFGET_NO_EXPORT int uf_file_is_locked(uf_file_lock_t *lock);
LIBUFGET_NO_EXPORT int uf_file_lock(uf_file_lock_t *lock, enum file_lock_flags flags);






LIBUFGET_NO_EXPORT int uf_table_meta(sqlite3 *db);
LIBUFGET_NO_EXPORT int uf_table_drop(sqlite3 *db, const char *table);
LIBUFGET_NO_EXPORT int uf_table_matrices(sqlite3 *db);

typedef union {
    double d;
    complex double z;
} value_t;

typedef size_t (*get_int_func) (void *array, size_t pos);
typedef value_t (*get_val_func) (void* array, size_t pos);
typedef void (*set_int_func) (void *array, size_t pos, size_t value);
typedef void (*set_val_func) (void *array, size_t pos, value_t value);

LIBUFGET_NO_EXPORT size_t index_size(uf_index_t p);
LIBUFGET_NO_EXPORT size_t value_size(uf_field_t f, uf_precision_t p);

LIBUFGET_NO_EXPORT get_int_func get_index(uf_index_t p);
LIBUFGET_NO_EXPORT set_int_func set_index(uf_index_t p);
LIBUFGET_NO_EXPORT get_val_func get_value(uf_field_t f, uf_precision_t p);
LIBUFGET_NO_EXPORT set_val_func set_value(uf_field_t f, uf_precision_t p);

LIBUFGET_NO_EXPORT int index_type_ok(uf_index_t, int64_t);

#ifndef FREE
#define FREE(X) do { free(X); X = NULL; } while(0)
#endif

#ifndef MAX
#define MAX(A,B) (((A)<(B))?(B):(A))
#endif

/* Column in the SQL table  */
#define COL_ID     0
#define COL_GROUP  1
#define COL_NAME   2
#define COL_NROWS  3
#define COL_NCOLS  4
#define COL_NNZ 5
#define COL_NZERO 6
#define COL_PATTERN_SYMMETRY 7
#define COL_NUMERICAL_SYMMETRY 8
#define COL_ISBINARY 9
#define COL_ISREAL 10
#define COL_ISCOMPLEX 11
#define COL_NNZDIAG 12
#define COL_POSDEF 13
#define COL_AMD_LNZ 14
#define COL_AMD_FLOPS 15
#define COL_AMD_VNZ 16
#define COL_AMD_RNZ 17
#define COL_NBLOCKS 18
#define COL_SPRANK 19
#define COL_RBTYPE 20
#define COL_CHOLCAND 21
#define COL_NCC 22
#define COL_ISND 23
#define COL_ISGRAPH 24
#define COL_LOWERBANDWIDTH 25
#define COL_UPPERBANDWIDTH 26
#define COL_RCM_LOWERBANDWIDTH 27
#define COL_RCM_UPPERBANDWIDTH 28
#define COL_XMIN_REAL 29
#define COL_XMIN_IMAG 30
#define COL_XMAX_REAL 31
#define COL_XMAX_IMAG 32

#define COL_L_ID        0
#define COL_L_LOCALPATH 1

typedef enum {
	MTX_UNKNOWN = 0,
	MTX_DENSE = 1,
	MTX_COORD = 2
} mtx_storage_t;

#define MTX_IS_DENSE(c)	((*c).store_type == MTX_DENSE)
#define MTX_IS_COORD(c)	((*c).store_type == MTX_COORD)

typedef enum {
	MTX_GENERAL = 0,
	MTX_SYMMETRIC,
	MTX_SKEWSYMMETRIC,
	MTX_HERMITIAN
} mtx_symmetry_t;

#define MTX_IS_GENERAL(m)	((*m).symmetry == MTX_GENERAL)
#define MTX_IS_UNSYMMETRIC(m)	((*m).symmetry == MTX_GENERAL)
#define MTX_IS_SYMMETRIC(m)	((*m).symmetry == MTX_SYMMETRIC)
#define MTX_IS_SKEWSYMMETRIC(m)	((*m).symmetry == MTX_SKEWSYMMETRIC)
#define MTX_IS_HERMITIAN(m)	((*m).symmetry == MTX_HERMITIAN)

typedef struct {
	int64_t cols;
	int64_t rows;
	int64_t nnz;
	mtx_storage_t store_type;
	mtx_symmetry_t symmetry;
	uf_field_t data_type;
} mtx_info_t ;

// struct _uf_io_file_t;
// mtx_info_t * uf_matrix_read_info(struct _uf_io_file_t* file);

#endif /* end of include guard: UF_INTERNAL_H */
