/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) Martin Koehler, 2015-2022
 * Copyright (C) Martin Koehler, Jonas Schulze, 2023
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <stdint.h>
#include <curl/curl.h>
#include <sqlite3.h>
#include <matio.h>
#include <archive.h>
#include <archive_entry.h>

#include <signal.h>
#include <setjmp.h>

#include "libufget.h"
#include "uf_internal.h"
#include "io/io.h"

uf_matrix_t * uf_matrix_new(void)
{
    uf_matrix_t * mat;

    mat = (uf_matrix_t *) calloc(1, sizeof(uf_matrix_t));
    if ( mat == NULL ) return NULL;

    mat->pimpl = (uf_matrix_pimpl *) calloc(1, sizeof(uf_matrix_pimpl));
    if ( mat->pimpl == NULL ) return NULL;

    return mat;
}

static void uf_matrix_pimpl_free(uf_matrix_pimpl * pimpl) {
    if ( pimpl == NULL ) return;
    FREE(pimpl);
}

void uf_matrix_free(uf_matrix_t * matrix)
{
    if ( matrix == NULL) return;
    uf_matrix_pimpl_free(matrix->pimpl);
    FREE(matrix);
}

void uf_matrix_print(uf_matrix_t const* matrix, int brief)
{
    uf_matrix_fprint(stdout, matrix, brief);
}

void uf_matrix_fprint(FILE * stream, uf_matrix_t const* matrix, int brief)
{
    fprintf(stream, "ID:                     : %d\n", matrix->id);
    fprintf(stream, "Group                   : %s\n",matrix->group_name);
    fprintf(stream, "Name                    : %s\n",matrix->name );
    fprintf(stream, "Rows                    : %" PRId64 "\n",matrix->rows );
    fprintf(stream, "Columns                 : %" PRId64 "\n",matrix->cols );
    fprintf(stream, "Non Zeros               : %" PRId64 "\n",matrix->nnz);
    if (!brief){
    fprintf(stream, "Zeros stored            : %" PRId64 "\n",matrix->nzero );
    fprintf(stream, "Pattern Symmetry        : %lg\n",matrix->pattern_symmetry );
    fprintf(stream, "Numerical Symmetry      : %lg\n",matrix->numerical_symmetry );
    fprintf(stream, "Binary                  : %d\n",matrix->isBinary );
    fprintf(stream, "Real                    : %d\n",matrix->isReal );
    fprintf(stream, "Complex                 : %d\n",matrix->isComplex );
    fprintf(stream, "nnzdiag                 : %" PRId64 "\n",matrix->nnzdiag );
    fprintf(stream, "posdef                  : %d\n",matrix->posdef );
    fprintf(stream, "amd_lnz                 : %" PRId64 "\n",matrix->amd_lnz );
    fprintf(stream, "amd_flops               : %" PRId64 "\n",matrix->amd_flops );
    fprintf(stream, "amd_vnz                 : %" PRId64 "\n",matrix->amd_vnz );
    fprintf(stream, "amd_rnz                 : %" PRId64 "\n",matrix->amd_rnz );
    fprintf(stream, "nblocks                 : %" PRId64 "\n",matrix->nblocks );
    fprintf(stream, "sprank                  : %" PRId64 "\n",matrix->sprank );
    fprintf(stream, "RBtype                  : %c%c%c\n",matrix->RBtype[0], matrix->RBtype[1], matrix->RBtype[2] );
    fprintf(stream, "cholcand                : %d\n",matrix->cholcand );
    fprintf(stream, "ncc                     : %" PRId64 "\n",matrix->ncc );
    fprintf(stream, "isND                    : %d\n",matrix->isND );
    fprintf(stream, "isGraph                 : %d\n",matrix->isGraph);
    fprintf(stream, "lowerbandwidth          : %" PRId64 "\n",matrix->lowerbandwidth );
    fprintf(stream, "upperbandwidth          : %" PRId64 "\n",matrix->upperbandwidth );
    fprintf(stream, "rcm_lowerbandwidth      : %" PRId64 "\n",matrix->rcm_lowerbandwidth );
    fprintf(stream, "rcm_upperbandwidth      : %" PRId64 "\n",matrix->rcm_upperbandwidth );
    fprintf(stream, "xmin                    : %lg + %lg i\n",matrix->xmin_real, matrix->xmin_imag );
    fprintf(stream, "xmax                    : %lg + %lg i\n",matrix->xmax_real, matrix->xmax_imag );
    }
}

struct curl_prg_data {
    char const* name;
    time_t start;
};

/*-----------------------------------------------------------------------------
 *  Curl Helper
 *-----------------------------------------------------------------------------*/
static int xferinfo(void *p, curl_off_t dltotal, curl_off_t dlnow, curl_off_t ultotal, curl_off_t ulnow)
{
    static time_t __thread lastcall= 0;
    struct curl_prg_data * dat = (struct curl_prg_data * ) p;
    time_t curcall = time(NULL);
    if ( dlnow <= 0 ) dlnow = 1; 
    if ( dltotal <= 0) dltotal = 1; 
    if ( lastcall < curcall ){
        long speed;
        if (curcall == dat->start) {
            speed = 0;
        } else {
            speed = dlnow / (curcall - dat->start);
        }
        int percent = (int) ( 100.0 * (double) dlnow / (double) dltotal);
        fprintf(stderr, "Downloading Matrix (%s) %3d%% (%06ldKiB / %06ldKiB) @ %8ld KiB/s    \r",(char const*) dat->name, percent, (long) dlnow/1024, (long) dltotal/1024, (long) speed/1024);
        fflush(stderr);
        lastcall = curcall;
    }
    return 0;
}

static int older_progress(void *p,  double dltotal, double dlnow, double ultotal, double ulnow)
{
  return xferinfo(p, (curl_off_t)dltotal, (curl_off_t)dlnow, (curl_off_t)ultotal,(curl_off_t)ulnow);
}

static sigjmp_buf curl_download_jmp;
static int got_signum = -1;

static void handle_signal(int signum) {
    got_signum = signum;
    siglongjmp(curl_download_jmp, 1);
}

char * uf_matrix_filename(uf_matrix_t const* mat)
{
    size_t local_len, len_group, len_name;
    char * localpath;

    if ( mat == NULL) return NULL;

    len_group = strlen(mat->group_name);
    len_name = strlen(mat->name);


    local_len = strlen(mat->pimpl->col->cache_dir) + len_name + len_group + 100;
    localpath = (char *) malloc(sizeof(char) * local_len);
    snprintf(localpath, local_len, "%s/MM/%s/%s.tar.gz", mat->pimpl->col->cache_dir, mat->group_name, mat->name);

    return localpath;
}

/*-----------------------------------------------------------------------------
 *  Download a matrix
 *-----------------------------------------------------------------------------*/
int  uf_matrix_get(uf_matrix_t const* mat)
{
    char *url = NULL;
    char *localpath = NULL;
    char *downloadpath = NULL;
    size_t len_base, len_group, len_name, local_len;
    CURL *curl = NULL;
    char errbuf[CURL_ERROR_SIZE];
    FILE *output = NULL;
    struct curl_prg_data dat;
    void (*oldsig_SIGINT)(int) = NULL;
    void (*oldsig_SIGTERM)(int) = NULL;
    void (*oldsig_SIGQUIT)(int) = NULL;

    int exc_handler = 0;
    char *lockfile = NULL;
    char *lockfile_desc = NULL;
    uf_file_lock_t * lock = NULL;


    /* Prepare Path */
    len_base = strlen(mat->pimpl->col->baseurl);
    len_group = strlen(mat->group_name);
    len_name = strlen(mat->name);


    len_base += len_group + len_name +100;
    local_len = strlen(mat->pimpl->col->cache_dir) + len_name + len_group + 100;

    url = (char *) malloc(sizeof(char) * (len_base));
    localpath = (char *) malloc(sizeof(char) * local_len);
    downloadpath = (char *) malloc(sizeof(char) * local_len);
    lockfile  = (char *) malloc(sizeof(char) * local_len);
    lockfile_desc  = (char *) malloc(sizeof(char) * local_len);



    snprintf(url, len_base, "%s/MM/%s/%s.tar.gz", mat->pimpl->col->baseurl, mat->group_name, mat->name);
    snprintf(localpath, local_len, "%s/MM/%s/", mat->pimpl->col->cache_dir, mat->group_name);


    if (uf_file_mkdir(localpath, mat->pimpl->col->dir_mode)) {
        DPRINTF(1, "mkdir (%s) failed.\n", localpath);
        goto failed;
    }
    snprintf(downloadpath, local_len, "%s/MM/%s/%s.tar.gz", mat->pimpl->col->cache_dir, mat->group_name, mat->name);
    snprintf(lockfile, local_len, "%s/MM/%s/%s.lock", mat->pimpl->col->cache_dir, mat->group_name, mat->name);
    snprintf(lockfile_desc, local_len, "Lock for %s/%s", mat->group_name, mat->name);

    lock = uf_file_lock_init(lockfile, lockfile_desc);
    FREE(lockfile);
    FREE(lockfile_desc);
    lockfile = NULL;
    lockfile_desc = NULL;

    uf_file_is_locked(lock);
    if ( uf_file_lock(lock, FILE_LOCK_WAIT) != UF_COLLECTION_LOCK_OK) {
        DPRINTF(1, "Failed to lock database.\n");
        goto failed;
    }

    if (uf_file_exist(downloadpath) ) {
        DPRINTF(3, "Matrix %s/%s was already downloaded. (%s)\n", mat->name, mat->group_name, downloadpath);
        uf_file_unlock(lock);
        uf_file_lock_free(lock);
        FREE(downloadpath);
        FREE(localpath);
        FREE(url);
        return 0;
    }

    oldsig_SIGINT = signal(SIGINT, handle_signal);
    oldsig_SIGTERM = signal(SIGTERM, handle_signal);
    oldsig_SIGQUIT = signal(SIGQUIT, handle_signal);

    if ( sigsetjmp(curl_download_jmp, 1) == 1 ) {
        exc_handler = 1;
        goto failed;
    }

    DPRINTF(1, "Download matrix from %s to %s\n", url, downloadpath);

    output = fopen(downloadpath, "w");

    if ( !output) {
        DPRINTF(1, "Opening %s for writing failed.\n", downloadpath);
        goto failed;
    }
    if (chmod(downloadpath, mat->pimpl->col->file_mode) != 0) {
        DPRINTF(1,"Failed to chmod %s to %04o\n", downloadpath, mat->pimpl->col->file_mode );
    }

    curl = curl_easy_init();
    if ( !curl ) {
        DPRINTF(1, "Error initializing CURL.\n");
        goto failed;
    }


    dat.name = mat->name;
    dat.start = time(NULL);
    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, output);
    curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errbuf);
    curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1);

    if ( uf_get_verbose_level > 0 ) {
        curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, older_progress);
        curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, &dat);
#if LIBCURL_VERSION_NUM >= 0x072000
        curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, xferinfo);
        curl_easy_setopt(curl, CURLOPT_XFERINFODATA, &dat);
#endif
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0);
    }

    CURLcode res = curl_easy_perform(curl);
    if ( res ) {
        fflush(stdout);
        fflush(stderr);
        DPRINTF(1, "\nFailed to get %s - %s\n", url, errbuf);
        fflush(stdout);
        fflush(stderr);
        goto failed;
    }
    signal(SIGINT, oldsig_SIGINT);
    signal(SIGTERM, oldsig_SIGTERM);
    signal(SIGQUIT, oldsig_SIGQUIT);

    /* New line after download  */
    DPRINTF(1, "\n");
    curl_easy_cleanup(curl);
    curl_global_cleanup();
    fclose(output);

    FREE(downloadpath);
    FREE(localpath);
    FREE(url);

    uf_file_unlock(lock);
    uf_file_lock_free(lock);

    return 0;

failed:
    if ( output != NULL ) { fclose (output); }
    if ( downloadpath != NULL ) {
        remove(downloadpath);
        FREE(downloadpath);
    }
    FREE(lockfile);
    FREE(lockfile_desc);
    if ( lock ) {
        uf_file_unlock(lock);
        uf_file_lock_free(lock);
        lock = NULL;
    }
    FREE(localpath);
    FREE(url);
    if ( curl != NULL ) curl_easy_cleanup(curl);
    curl_global_cleanup();

    signal(SIGINT, oldsig_SIGINT);
    signal(SIGTERM, oldsig_SIGTERM);
    signal(SIGQUIT, oldsig_SIGQUIT);
    if ( exc_handler ){
        kill(getpid(), got_signum);
    }

    return -1;
}


/*-----------------------------------------------------------------------------
 *  Matrix from Statement
 *-----------------------------------------------------------------------------*/
void uf_matrix_from_stmt(uf_matrix_t *matrix, sqlite3_stmt *res)
{
/* Extra information from DB   */
    matrix -> id = sqlite3_column_int(res, COL_ID);
    strncpy(matrix->group_name, (const char *) sqlite3_column_text(res, COL_GROUP), 1023);
    strncpy(matrix->name, (const char *) sqlite3_column_text(res, COL_NAME), 1023);
    matrix->rows = sqlite3_column_int64(res, COL_NROWS);
    matrix->cols = sqlite3_column_int64(res, COL_NCOLS);
    matrix->nnz   = sqlite3_column_int64(res, COL_NNZ);
    matrix->nzero = sqlite3_column_int64(res, COL_NZERO);
    matrix->pattern_symmetry = sqlite3_column_double(res, COL_PATTERN_SYMMETRY);
    matrix->numerical_symmetry = sqlite3_column_double(res, COL_NUMERICAL_SYMMETRY);
    matrix->isBinary  = sqlite3_column_int(res, COL_ISBINARY);
    matrix->isReal    = sqlite3_column_int(res, COL_ISREAL);
    matrix->isComplex   = sqlite3_column_int(res, COL_ISCOMPLEX);
    matrix->nnzdiag = sqlite3_column_int64(res, COL_NNZDIAG);
    matrix->posdef  = sqlite3_column_int(res, COL_POSDEF);
    matrix->amd_lnz = sqlite3_column_int64(res, COL_AMD_LNZ);
    matrix->amd_flops = sqlite3_column_int64(res, COL_AMD_FLOPS);
    matrix->amd_vnz = sqlite3_column_int64(res, COL_AMD_VNZ);
    matrix->amd_rnz = sqlite3_column_int64(res, COL_AMD_RNZ);
    matrix->nblocks = sqlite3_column_int64(res, COL_NBLOCKS);
    matrix->sprank  = sqlite3_column_int64(res, COL_SPRANK);
    strncpy(matrix->RBtype, (const char *) sqlite3_column_text(res, COL_RBTYPE), 3);
    matrix->cholcand  = sqlite3_column_int(res, COL_CHOLCAND);
    matrix->ncc      = sqlite3_column_int64(res, COL_NCC);
    matrix->isND = sqlite3_column_int(res, COL_ISND);
    matrix->isGraph = sqlite3_column_int(res, COL_ISGRAPH);
    matrix->lowerbandwidth = sqlite3_column_int64(res, COL_LOWERBANDWIDTH);
    matrix->upperbandwidth = sqlite3_column_int64(res, COL_UPPERBANDWIDTH);
    matrix->rcm_lowerbandwidth = sqlite3_column_int64(res, COL_RCM_LOWERBANDWIDTH);
    matrix->rcm_upperbandwidth = sqlite3_column_int64(res, COL_RCM_UPPERBANDWIDTH);
    matrix->xmin_real = sqlite3_column_double(res, COL_XMIN_REAL);
    matrix->xmin_imag = sqlite3_column_double(res, COL_XMIN_IMAG);
    matrix->xmax_real = sqlite3_column_double(res, COL_XMAX_REAL);
    matrix->xmax_imag = sqlite3_column_double(res, COL_XMAX_IMAG);
}


/* Copy matrix */
void uf_matrix_copy(uf_matrix_t const* src, uf_matrix_t *dst) {
    if (dst->pimpl) uf_matrix_pimpl_free(dst->pimpl);
    memcpy(dst, src, sizeof(uf_matrix_t));
    // deep copy:
    dst->pimpl = malloc(sizeof(uf_matrix_pimpl));
    dst->pimpl->col = src->pimpl->col;
}

/* Duplicate a Matrix */
uf_matrix_t * uf_matrix_dup(uf_matrix_t const* src) {
    uf_matrix_t * mat = malloc(sizeof(uf_matrix_t));
    mat->pimpl = NULL;
    uf_matrix_copy(src, mat);
    return mat;
}


