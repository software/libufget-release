/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) Martin Koehler, Jonas Schulze, 2023
 */

#include "libufget.h"
#include "uf_internal.h"

#define SET_VALUE(name, type) \
static void set_value_##name(void* _array, size_t pos, value_t value) \
{ \
    type* array = (type*) _array; \
    array[pos] = (type) value.d; \
}

SET_VALUE(f32, float)
SET_VALUE(f64, double)
SET_VALUE(c32, complex float)
SET_VALUE(c64, complex double)

#undef SET_VALUE

static set_val_func set_real(uf_precision_t p)
{
    switch (p) {
        case UF_F32: return set_value_f32;
        case UF_F64: return set_value_f64;
        default:
            DPRINTF(0, "Unknown real precision: %d\n", p);
            return NULL;
    }
}

static set_val_func set_complex(uf_precision_t p)
{
    switch (p) {
        case UF_F32: return set_value_c32;
        case UF_F64: return set_value_c64;
        default:
            DPRINTF(0, "Unknown real precision: %d\n", p);
            return NULL;
    }
}

set_val_func set_value(uf_field_t f, uf_precision_t p)
{
    if ( f == UF_REAL || f == UF_PATTERN ) return set_real(p);
    return set_complex(p);
}

/* Getters ************************************************************/

#define GET_REAL_VALUE(name, type) \
static value_t get_value_##name(void* _array, size_t pos) \
{ \
    value_t v; \
    type* array = (type*) _array; \
    v.d = (double) array[pos]; \
    return v; \
}

GET_REAL_VALUE(f32, float)
GET_REAL_VALUE(f64, double)

#undef GET_REAL_VALUE


#define GET_COMPLEX_VALUE(name, type) \
static value_t get_value_##name(void* _array, size_t pos) \
{ \
    value_t v; \
    type* array = (type*) _array; \
    v.z = (complex double) array[pos]; \
    return v; \
}

GET_COMPLEX_VALUE(c32, complex float)
GET_COMPLEX_VALUE(c64, complex double)

#undef GET_COMPLEX_VALUE

static get_val_func get_real(uf_precision_t p)
{
    switch (p) {
        case UF_F32: return get_value_f32;
        case UF_F64: return get_value_f64;
        default:
            DPRINTF(0, "Unknown real precision: %d\n", p);
            return NULL;
    }
}

static get_val_func get_complex(uf_precision_t p)
{
    switch (p) {
        case UF_F32: return get_value_c32;
        case UF_F64: return get_value_c64;
        default:
            DPRINTF(0, "Unknown real precision: %d\n", p);
            return NULL;
    }
}

get_val_func get_value(uf_field_t f, uf_precision_t p)
{
    if ( f == UF_REAL || f == UF_PATTERN ) return get_real(p);
    return get_complex(p);
}

/* Size info ************************************************************/

static size_t real_size(uf_precision_t p)
{
    switch (p) {
        case UF_F32: return sizeof(float);
        case UF_F64: return sizeof(double);
        default:
            DPRINTF(0, "Unknown real precision: %d\n", p);
            return 0;
    }
}

static size_t complex_size(uf_precision_t p)
{
    switch (p) {
        case UF_F32: return sizeof(complex float);
        case UF_F64: return sizeof(complex double);
        default:
            DPRINTF(0, "Unknown complex precision: %d\n", p);
            return 0;
    }
}

size_t value_size(uf_field_t f, uf_precision_t p)
{
    if ( f == UF_REAL || f == UF_PATTERN ) return real_size(p);
    return complex_size(p);
}
