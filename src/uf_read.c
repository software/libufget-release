/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) Martin Koehler, 2015-2022
 * Copyright (C) Martin Koehler, Jonas Schulze, 2023
 */


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <complex.h>
#include <ctype.h>

#include <curl/curl.h>
#include <sqlite3.h>
#include <matio.h>
#include <archive.h>
#include <archive_entry.h>

#include "libufget.h"
#include "uf_internal.h"
#include "io/io.h"

struct _libarchive_read_t {
    size_t size;
    int64_t offset;
    const void *buffer;
    size_t pos;
    struct archive * a;
};

static int _libarchive_close(void **data)
{
    struct _libarchive_read_t * idata = (struct _libarchive_read_t *) *data;
   archive_read_close(idata->a);
#if ARCHIVE_VERSION_NUMBER < 3000000
   archive_read_finish(idata->a);
#else
   archive_read_free(idata->a);
#endif

    FREE(*data);
    *data = NULL;
    return 0;
}


static size_t _libarchive_read(void *_data, void *buf, size_t len)
{
    struct _libarchive_read_t *data = (struct _libarchive_read_t*) _data;
    int r;
    size_t remain;
    const char *cbuffer;
    if ( data->pos == 0  && data->offset == 0) {
        r = archive_read_data_block(data->a, &(data->buffer), &(data->size), &(data->offset));
    } else if (data->pos == data->size) {
        r = archive_read_data_block(data->a, &(data->buffer), &(data->size), &(data->offset));
        data->pos = 0;
    } else {
        r = ARCHIVE_OK;
    }
    // printf("size: %ld \t pos: %ld \t offset: %ld\n", data->size, data->pos, data->offset);
    if (r!=ARCHIVE_OK) return 0;
    cbuffer = data->buffer;
    remain = data->size - data->pos;
    if (len >= remain)
        len = remain;
    memcpy(buf, &cbuffer[data->pos], len);
    data->pos+=len;
    return len;
}

struct _compressed_io_handler_t libarchive_read_handler = {
    .extension = "",
    .type = UF_IO_FILE_HANDLE,
    .magic = "",
    .open = NULL,
    .close = _libarchive_close,
    .read  = _libarchive_read,
    .write = NULL
};

static uf_io_file_t* _archive_open(struct archive *a )
{
	uf_io_file_t  *f;
    struct _libarchive_read_t *data;

	f = (uf_io_file_t *) malloc ( sizeof(uf_io_file_t));
	if ( f == NULL ) {
		DPRINTF(3, "Allocation of the mess_file object failed.\n");
		return NULL;
	}
    data =  (struct _libarchive_read_t *) malloc(sizeof(struct _libarchive_read_t) * (1));
    if ( data == NULL) {
        DPRINTF(3, "Failed to allocate internal data\n");
        return NULL;
    }
    data->size = 0;
    data->offset =  0;
    data->pos = 0;
    data->a = a;

	/*-----------------------------------------------------------------------------
	 *  Open the file
	 *-----------------------------------------------------------------------------*/
	f->pos = 0;
	f->avail = 0;
	f->data  = data;
    f->mode = UF_IO_FILE_READ;
	f->eof = 0;
    f->handler = &libarchive_read_handler;
	return f;
}

static uf_io_file_t * uf_matrix_open(uf_matrix_t const* mat)
{
    char *internal_filename;
    char *localpath;
    size_t len;
    struct archive *a;
    struct archive_entry         *entry;
    int r;

    if ( mat == NULL) return NULL;

    /* Get if not available  */
    uf_matrix_get(mat);

    len = 2*strlen(mat->name) + 15;
    internal_filename = (char *) malloc(sizeof(char) * (len));
    snprintf(internal_filename, len, "%s/%s.mtx", mat->name, mat->name);

    localpath = uf_matrix_filename(mat);

    a = archive_read_new();
    if (!a) {
        DPRINTF(3, "Initialize libarchive failed.\n");
        FREE(internal_filename);
        return NULL;
    }

   archive_read_support_format_all(a);
#if ARCHIVE_VERSION_NUMBER < 3000000
   archive_read_support_compression_all(a);
#else
   archive_read_support_filter_all(a);
#endif
#define ARCHIVE_BUFFER_SIZE 1024*10240
   if ((r = archive_read_open_filename(a, localpath, ARCHIVE_BUFFER_SIZE))){
       DPRINTF(3, "Failed to open - Error: %s\n", archive_error_string(a));
#if ARCHIVE_VERSION_NUMBER < 3000000
       archive_read_finish(a);
#else
       archive_read_free(a);
#endif
       FREE(localpath);
       FREE(internal_filename);
       return NULL;
   }

   while (1 ) {
        r = archive_read_next_header(a, &entry);
        if ( r == ARCHIVE_EOF)
            goto failed;
        if ( r != ARCHIVE_OK) {
            DPRINTF(3, "ERROR: %s\n", archive_error_string(a));
            goto failed;
        }

        // s = archive_entry_size(entry);
        if ( strcmp(archive_entry_pathname(entry), internal_filename) == 0) {
            // DPRINTF(3, "Found %s in file.\n", internal_filename);
            break;
        } else {
            archive_read_data_skip(a);
            continue;
        }
   }
   FREE(localpath);
   FREE(internal_filename);
   uf_io_file_t * file = _archive_open(a);
   return file;

failed:
   FREE(localpath);
   FREE(internal_filename);
   archive_read_close(a);
#if ARCHIVE_VERSION_NUMBER < 3000000
   archive_read_finish(a);
#else
   archive_read_free(a);
#endif
   return NULL;
}



/*-----------------------------------------------------------------------------
 *  MTX definitions
 *-----------------------------------------------------------------------------*/
#define LINE_LENGTH 1025
#define TOKEN_LENGTH 30

/* Sets the last entry of an array to value */
#define 	SET_LAST(array, len, value) 	(array)[(len)-1] = (value)
/* Definitions for the Matrix Market header */
#define MM_BANNER		"%%MatrixMarket"
#define MM_STR_MATRIX		"matrix"
#define MM_STR_COORD		"coordinate"
#define MM_STR_ARR		"array"
#define MM_STR_REAL		"real"
#define MM_STR_INT		"integer"
#define MM_STR_CPX		"complex"
#define MM_STR_PAT		"pattern"
#define MM_STR_GE		"general"
#define MM_STR_SYM		"symmetric"
#define MM_STR_SKSYM		"skew-symmetric"
#define MM_STR_HER		"hermitian"

static void _lowercase (char *str)
{
	char *p;
	if ( str == NULL) return;
	for (p=str; *p!='\0'; *p=(char) tolower((int) *p),p++);
}


/*-----------------------------------------------------------------------------
 *  Read and parse the header of an mtx file.
 *-----------------------------------------------------------------------------*/
static mtx_info_t * uf_matrix_read_info(uf_io_file_t * file)
{
	char line[LINE_LENGTH];
	char mmbanner[TOKEN_LENGTH];
	char mtx[TOKEN_LENGTH];
	char type[TOKEN_LENGTH];
	char datatype[TOKEN_LENGTH];
	char shape[TOKEN_LENGTH];
	int num_items_read;
    char * cret;
    mtx_info_t *matrix_info;

	int64_t N,M,NZ;

    matrix_info = (mtx_info_t *) malloc(sizeof(mtx_info_t) * (1));
	if ( matrix_info == NULL ) {
		DPRINTF(3, "Failed to allocate matrix_info.\n");
		return NULL;
	}

	cret = uf_io_gets(line, LINE_LENGTH,file);
	if ( !cret ) {
		DPRINTF(3, "Error reading the first line of the file.\n");
        FREE(matrix_info);
		return NULL;
	}

	// %%MatrixMarket matrix type datatype shape
	if (sscanf(line, "%s %s %s %s %s", mmbanner, mtx, type, datatype, shape) != 5){
        DPRINTF(3, "Failed to parse the header line: %s\n", line);
        FREE(matrix_info);
        return NULL;
	}

	// prevent memory overflow
	SET_LAST(mmbanner, TOKEN_LENGTH, '\0');
	SET_LAST(mtx, TOKEN_LENGTH, '\0');
	SET_LAST(type, TOKEN_LENGTH, '\0');
	SET_LAST(datatype, TOKEN_LENGTH, '\0');
	SET_LAST(shape, TOKEN_LENGTH, '\0');

	_lowercase(mtx);
	_lowercase(type);
	_lowercase(datatype);
	_lowercase(shape);

	/* check if the matrix market banner exists */
	if (strncmp(mmbanner, MM_BANNER, strlen(MM_BANNER)) != 0){
		DPRINTF(3, "wrong header information: %s\n", mmbanner);
        FREE(matrix_info);
		return NULL;
	}

	/* parse the rest of the 1st line*/
	if (strncmp(mtx, MM_STR_MATRIX, strlen(MM_STR_MATRIX)) != 0){
		DPRINTF(3, "wrong header information: %s\n", mtx);
        FREE(matrix_info);
		return NULL;
	}

	if (strncmp(type, MM_STR_ARR,strlen(MM_STR_ARR)) == 0){
		matrix_info->store_type = MTX_DENSE;
	} else if (strncmp(type, MM_STR_COORD, strlen(MM_STR_COORD)) == 0){
		matrix_info->store_type = MTX_COORD;
	} else {
	    DPRINTF(3, "wrong storage format: %s\n", type );
        FREE(matrix_info);
		return NULL;
 	}

	if (strncmp(datatype, MM_STR_REAL,strlen(MM_STR_REAL)) == 0) {
		matrix_info->data_type = UF_REAL;
	} else if (strncmp(datatype, MM_STR_INT, strlen(MM_STR_INT)) == 0){
		matrix_info->data_type = UF_REAL;
	} else if (strncmp(datatype, MM_STR_CPX, strlen(MM_STR_CPX)) == 0){
		matrix_info->data_type = UF_COMPLEX;
	}
	else if (strncmp(datatype, MM_STR_PAT, strlen(MM_STR_PAT)) == 0) {
        // printf("Read pattern\n");
		matrix_info->data_type = UF_PATTERN;
	} else {
        DPRINTF(3,"wrong datatype: %s\n", datatype);
        FREE(matrix_info);
		return NULL;
 	}

	if (strncmp(shape, MM_STR_GE,strlen(MM_STR_GE)) == 0){
		matrix_info->symmetry = MTX_GENERAL;
	} else if (strncmp(shape, MM_STR_SYM, strlen(MM_STR_SYM)) == 0){
		matrix_info->symmetry = MTX_SYMMETRIC;
	} else if (strncmp(shape, MM_STR_SKSYM, strlen(MM_STR_SKSYM)) == 0) {
		matrix_info->symmetry = MTX_SKEWSYMMETRIC;
	} else if (strncmp(shape, MM_STR_HER, strlen(MM_STR_HER)) == 0) {
		matrix_info->symmetry = MTX_HERMITIAN;
	}  else {
        DPRINTF(3, "Invalid symmetry information: %s \n", shape);
        FREE(matrix_info);
		return NULL;
 	}

	// read until matrix size is read.
	do{
		cret = uf_io_gets(line, LINE_LENGTH, file);
		if ( !cret  ) {
			DPRINTF(3, "error while reading a line from file");
            FREE(matrix_info);
			return NULL;
		}
	} while (line[0] == '%');


	if (MTX_IS_COORD(matrix_info)){
		do {
			long _n, _m, _nz;
			num_items_read = sscanf(line, "%ld %ld %ld", &_n, &_m, &_nz);
			if (num_items_read != 3) {
				cret = uf_io_gets(line, LINE_LENGTH, file);
				if ( !cret ) {
					 DPRINTF(3, "error while reading a line from file\n");
                     FREE(matrix_info);
					 return  NULL;
				}
			}
			N = _n;
			M = _m;
			NZ = _nz;
		} while (num_items_read != 3);
	}else{
		NZ = 0;
		do {
			long _n, _m;
			num_items_read = sscanf(line,"%ld %ld", &_n, &_m);
			if (num_items_read != 2) {
				cret = uf_io_gets(line, LINE_LENGTH, file);
				if ( !cret   ) {
					DPRINTF(3, "error while reading a line from file\n");
                    FREE(matrix_info);
					return NULL;
				}
			}
			N = _n;
			M = _m;
		} while (num_items_read != 2);
		NZ = N * M;
	}

	matrix_info->rows = N;
	matrix_info->cols = M;
	matrix_info->nnz = NZ;
	return matrix_info;
}

typedef void (*apply_symmetry_func) (value_t* v);

static
void apply_noop(value_t* v)
{}

static
void apply_conj(value_t* v)
{
    v->z = conj(v->z);
}

static
void apply_skewsymmetric(value_t* v)
{
    // Changing the complex member affects the double member as well.
    v->z = -v->z;
}

typedef int (*parser_func) (char* line, size_t* row, size_t* col, value_t* val);

static
int parse_line_pattern(char* line, size_t* r, size_t* c, value_t* v)
{
    int ok = sscanf(line, "%zu %zu\n", r, c) == 2;
    v->d = 1;
    return ok;
}

static
int parse_line_real(char* line, size_t* r, size_t* c, value_t* v)
{
    int ok = sscanf(line, "%zu %zu %lg\n", r, c, &(v->d)) == 3;
    return ok;
}

static
int parse_line_complex(char* line, size_t* r, size_t* c, value_t* v)
{
    double re, im;
    int ok = sscanf(line, "%zu %zu %lg %lg\n", r, c, &re, &im) == 4;
    v->z = re + im * I;
    return ok;
}


/*-----------------------------------------------------------------------------
 *  Read matrix
 *-----------------------------------------------------------------------------*/
int uf_matrix_coord_extended(
        uf_field_t *field
        , int64_t *nrows, int64_t *ncols, int64_t *nnz
        , void **rowptr, uf_index_t rowtype
        , void **colptr, uf_index_t coltype
        , void **values, uf_precision_t valtype
        , uf_matrix_t const* mat
        )
{
    mtx_info_t *mat_info =  NULL;
    uf_io_file_t *file = NULL;
    char line[LINE_LENGTH];
    char *cret = NULL;

    /* Open */
    file = uf_matrix_open(mat);
    if ( file == NULL) {
        DPRINTF(3, "Open matrix failed.\n");
        return -1;
    }

    /* Parse Header  */
    mat_info = uf_matrix_read_info(file);
    if (mat_info == NULL ){
        DPRINTF(3, "Failed to read MTX header.\n");
        uf_io_close(file);
        return -1;
    }

    /* Copy header  */
    *field = mat_info->data_type;
    *nrows = mat_info->rows;
    *ncols = mat_info->cols;
    *nnz = mat_info->nnz;

    /* Safety checks */
    if (!index_type_ok(rowtype, *nrows - 1)) {
        DPRINTF(3, "Row index type not large enough to store nrows-1.\n");
        goto failed;
    }
    if (!index_type_ok(coltype, *ncols - 1)) {
        DPRINTF(3, "Column index type not large enough to store ncols-1.\n");
        goto failed;
    }

    /* Create setter functions and other metadata */
    size_t row_index_size = index_size(rowtype);
    size_t col_index_size = index_size(coltype);
    size_t val_size = value_size(*field, valtype);
    set_int_func set_row = set_index(rowtype);
    set_int_func set_col = set_index(coltype);
    set_val_func set_val = set_value(*field, valtype);

    if (!row_index_size || !set_row) {
        DPRINTF(0, "Invalid row index type\n");
        goto failed;
    }
    if (!col_index_size || !set_col) {
        DPRINTF(0, "Invalid column index type\n");
        goto failed;
    }
    if (!val_size || !set_val) {
        DPRINTF(0, "Invalid combination of uf_field_t and uf_precision_t\n");
        goto failed;
    }

    // printf("field: %d, nrows: %d, ncols: %d, nnz: %d\n", *field, *nrows, *ncols, *nnz);
    // printf("Intsize: %d\n", intsize*8);
    if (!MTX_IS_COORD(mat_info)) {
        DPRINTF(3, "Only coordinate matrices are supported at the moment.\n");
        goto failed;
    }

    /* Setup parser */
    parser_func parse_line;
    char const* parse_error;
    apply_symmetry_func apply_hermitian;
    if (*field == UF_REAL) {
        parse_line = parse_line_real;
        parse_error = "cannot read value for real format in line %ld  - \"%s\"\n";
        apply_hermitian = apply_noop; // same as symmetric, i.e. "do nothing"
    } else if (*field == UF_COMPLEX) {
        parse_line = parse_line_complex;
        parse_error = "cannot read value for complex format in line %ld  - \"%s\"\n";
        apply_hermitian = apply_conj;
    } else if (*field == UF_PATTERN) {
        parse_line = parse_line_pattern;
        parse_error = "cannot read value for pattern format in line %ld  - \"%s\"\n";
        apply_hermitian = apply_noop; // pattern entries never change
    } else {
        DPRINTF(3, "Unknown data type.\n");
        goto failed;
    }

    /* Read Sparse Data */
    void *irowptr, *icolptr;
    size_t innz;
    if (MTX_IS_GENERAL(mat_info))
        innz = (size_t) mat_info->nnz;
    else
        innz = 2  * (size_t) mat_info->nnz;

    irowptr =  malloc(row_index_size * innz);
    icolptr =  malloc(col_index_size * innz);
    if ( irowptr == NULL || icolptr == NULL ){
        FREE(irowptr); FREE(icolptr);
        DPRINTF(3, "Allocate Row/Column pointer failed.\n");
        goto failed;
    }
    innz=0;

    value_t _v;
    size_t _r, _c, i;
    int ok;

    void* ivalues = malloc(val_size * ((size_t)mat_info->nnz) * 2);
    if (ivalues == NULL) {
        FREE(irowptr); FREE(icolptr);
        DPRINTF(3, "Allocate values failed.\n");
        goto failed;
    }

    for (i=0; i < mat_info->nnz; i++){
        cret = uf_io_gets(line, LINE_LENGTH,file);
        ok = parse_line(line, &_r, &_c, &_v);
        if (!ok || !cret){
            DPRINTF(3, parse_error, (long) i,line);
            FREE(irowptr); FREE(icolptr); FREE(ivalues);
            goto failed;
        }
        set_row(irowptr, innz, _r - 1);
        set_col(icolptr, innz, _c - 1);
        set_val(ivalues, innz, _v);

        if ( MTX_IS_SYMMETRIC(mat_info) && _r != _c ){
            // printf("Symmetric\n");
            innz++;
            set_row(irowptr, innz, _c - 1);
            set_col(icolptr, innz, _r - 1);
            set_val(ivalues, innz, _v);
        }
        if ( MTX_IS_HERMITIAN(mat_info) && _r != _c ){
            innz++;
            apply_hermitian(&_v);
            set_row(irowptr, innz, _c - 1);
            set_col(icolptr, innz, _r - 1);
            set_val(ivalues, innz, _v);
        }
        if ( MTX_IS_SKEWSYMMETRIC(mat_info) && _r != _c ){
            innz++;
            apply_skewsymmetric(&_v);
            set_row(irowptr, innz, _c - 1);
            set_col(icolptr, innz, _r - 1);
            set_val(ivalues, innz, _v);
        }
        innz ++;
    }
    *nnz = (int64_t) innz;
    // printf("innz: %ld\n", innz);
    irowptr = realloc(irowptr, row_index_size * innz);
    icolptr = realloc(icolptr, col_index_size * innz);
    ivalues = realloc(ivalues, val_size * innz);
    *rowptr = irowptr;
    *colptr = icolptr;
    *values = ivalues;

    FREE(mat_info);
    uf_io_close(file);
    return 0;
failed:
    FREE(mat_info);
    uf_io_close(file);
    return -1;

}

/*-----------------------------------------------------------------------------
 *  Int32 and Int64 interface to the coordinate read function.
 *-----------------------------------------------------------------------------*/
int uf_matrix_coord_int32(uf_field_t *field, int32_t *nrows, int32_t *ncols, int32_t *nnz, int32_t **rowptr, int32_t **colptr, void **values, uf_matrix_t const* mat)
{
    int64_t _nrows = (int64_t) *nrows;
    int64_t _ncols = (int64_t) *ncols;
    int64_t _nnz = (int64_t) *nnz;
    int err = uf_matrix_coord_extended(
            field, &_nrows, &_ncols, &_nnz,
            (void **) rowptr, UF_I32,
            (void **) colptr, UF_I32,
            (void **) values, UF_F64,
            mat);
    *nrows = (int32_t) _nrows;
    *ncols = (int32_t) _ncols;
    *nnz = (int32_t) _nnz;
    return err;
}

int uf_matrix_coord_int64(uf_field_t *field, int64_t *nrows, int64_t *ncols, int64_t *nnz, int64_t **rowptr, int64_t **colptr, void **values, uf_matrix_t const* mat)
{
    return uf_matrix_coord_extended(
            field, nrows, ncols, nnz,
            (void **) rowptr, UF_I64,
            (void **) colptr, UF_I64,
            (void **) values, UF_F64,
            mat);
}

