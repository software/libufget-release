/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) Martin Koehler, 2015-2022
 * Copyright (C) Martin Koehler, Jonas Schulze, 2023
 */

#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>

#include "libufget.h"
#include "uf_internal.h"
#include "io/io.h"

#define LIBUFGET_OPEN_ZFS_MAGIC 0x2fc12fc1
#define LIBUFGET_ORACLE_ZFS_MAGIC 0xde
#if defined(__linux__)
#include <linux/magic.h>
#include <sys/statfs.h>
#ifndef UFS_MAGIC // described in man 2 statfs
#define UFS_MAGIC 0x00011954
#endif

// Some older kernels don't ship all definitions
#ifndef SMB2_SUPER_MAGIC // added in v5.17
#define SMB2_SUPER_MAGIC 0xfe534d42
#endif
#ifndef XFS_SUPER_MAGIC // added in v4.20
#define XFS_SUPER_MAGIC 0x58465342
#endif

#elif defined(BSD) || defined(__APPLE__)
#include <sys/param.h>
#include <sys/mount.h>
#define UFS_MAGIC 0x00011954

// Copied from linux/magic.h
#define EXT2_SUPER_MAGIC 0xEF53
#define BTRFS_SUPER_MAGIC 0x9123683E
#define BTRFS_TEST_MAGIC 0x73727279
#define AFS_SUPER_MAGIC 0x5346414F
#define NFS_SUPER_MAGIC 0x6969
#define XFS_SUPER_MAGIC 0x58465342
#define SMB_SUPER_MAGIC 0x517b
#define SMB2_SUPER_MAGIC 0xfe534d42
#endif

#ifdef HAVE_INOTIFY
#include <sys/inotify.h>
#include <limits.h>
#define BUF_LEN (10 * (sizeof(struct inotify_event) + NAME_MAX + 1))

#endif

#define LOCK_DEBUG_LEVEL 2

static int should_use_inotify(const char *filename)
{
#ifdef HAVE_INOTIFY
    char* filename_dup = strdup(filename);
    char* dir = dirname(filename_dup);
    struct statfs buf;
    if (statfs(dir, &buf) != 0) {
        int err = errno;
        DPRINTF(LOCK_DEBUG_LEVEL, "statfs on %s failed. err = %d errmsg = %s\n", dir, err, strerror(err));
        FREE(filename_dup);
        return 0;
    }
    FREE(filename_dup);
    switch (buf.f_type) {
        case EXT2_SUPER_MAGIC: // same for EXT[2-4]
        case BTRFS_SUPER_MAGIC:
        case BTRFS_TEST_MAGIC:
        case XFS_SUPER_MAGIC:
        case UFS_MAGIC:
        case LIBUFGET_OPEN_ZFS_MAGIC:
        case LIBUFGET_ORACLE_ZFS_MAGIC:
            return 1;
        case AFS_SUPER_MAGIC:
        case NFS_SUPER_MAGIC:
        case SMB_SUPER_MAGIC:
        case SMB2_SUPER_MAGIC:
        default:
            return 0;
    }
#else
    return 0;
#endif
}

uf_file_lock_t * uf_file_lock_init(const char * filename, const char * desc)
{
    uf_file_lock_t * lock = malloc(sizeof(uf_file_lock_t));
    if ( ! lock) return NULL;
    lock->fd = -1;
    lock->filename = strdup(filename);
    lock->desc = strdup(desc);
    lock->use_inotify = should_use_inotify(filename);
    if (lock->use_inotify) {
        DPRINTF(LOCK_DEBUG_LEVEL, "lock (filename = %s, desc = '%s') will use inotify\n", lock->filename, lock->desc);
    } else {
        DPRINTF(LOCK_DEBUG_LEVEL, "lock (filename = %s, desc = '%s') will not use inotify\n", lock->filename, lock->desc);
    }
    return lock;
}

void uf_file_lock_free(uf_file_lock_t * lock)
{
    FREE(lock->filename);
    FREE(lock->desc);
    FREE(lock);
    return;
}

/**
 * Unlock a previously locked file.
 */
void
uf_file_unlock(uf_file_lock_t * lock)
{
	if (lock->fd < 0)
        return;
    close(lock->fd);
    remove(lock->filename);
    lock->fd = -1;
    return;
}

/*
 * Checks if a file is locked.
 * Returns: - 0 if the file is unlocked
 *          - >0 if the file is locked on the same host, i.e. the pid of the locking process is returned
 *          - -1 if the file is locked but from another host.
 */
int
uf_file_is_locked(uf_file_lock_t *lock)
{
    int fd;
    char buffer[1025];
    char myhostname[257];

    gethostname(myhostname, 256);

    fd = open(lock->filename,O_RDONLY);
    if ( fd < 0) return 0;
    ssize_t r = read(fd, buffer, 1024);
    if (r>=0) buffer[r] = '\0';
    char *saveptr = NULL;
    char * hstname = strtok_r(buffer, ":", &saveptr);
    char * pidstr = strtok_r(NULL, ":", &saveptr);
    int pid = -1;
    if ( strcmp (hstname, myhostname) == 0 ) {
        pid = atoi(pidstr);
    }
    DPRINTF(LOCK_DEBUG_LEVEL, "lock (filename = %s, desc = '%s') held on %s by process %d\n", lock->filename, lock->desc, hstname, pid);
    close(fd);
    return pid;

}

static void
setcloexec(int fd)
{
  int f;

  f = fcntl(fd, F_GETFD);
  fcntl(fd, F_SETFD, (f|FD_CLOEXEC));
}

#ifdef HAVE_INOTIFY
static int wait_for_delete_inotify(const char *fn)
{
    int ifd, iwd;
    char buffer[BUF_LEN];
    ssize_t numRead = -1;
    char *p;
    struct inotify_event *evt;
    int deleted;
    do {
        if ( uf_file_exist(fn)){
            ifd = inotify_init();
            if ( ifd == -1 ) {
                DPRINTF(LOCK_DEBUG_LEVEL, "Failed to initialize inotify.\n");
                return -1;
            }
            iwd = inotify_add_watch(ifd, fn, IN_DELETE_SELF);
            if ( iwd == -1) {
                DPRINTF(LOCK_DEBUG_LEVEL, "Failed to watch lock-file (%s).\n", fn);
                close(ifd);
                return -1;

            }
            deleted = 0;
            for(;;) {
                numRead = read(ifd, buffer, BUF_LEN);
                if ( numRead <= 0) {
                    DPRINTF(LOCK_DEBUG_LEVEL, "waiting for lock failed.\n");
                    close(ifd);
                    return -1;
                }
                for ( p = buffer; p < buffer + numRead;) {
                    evt = (struct inotify_event *) p;

                    if ( evt-> mask & IN_DELETE_SELF || evt->mask & IN_IGNORED ) {
                        DPRINTF(LOCK_DEBUG_LEVEL+2, "Lock-file deleted.\n");
                        deleted = 1;
                        break;
                    }
                    p += sizeof(struct inotify_event) + evt->len;
                }
                if (deleted) break;
            }
            close(ifd);
            continue;
        }
        break;
    } while (1);

    return 0;
}
#endif

static void wait_for_delete(uf_file_lock_t* lock)
{
#ifdef HAVE_INOTIFY
    if (lock->use_inotify) {
        wait_for_delete_inotify(lock->filename);
        return;
    }
#endif
    struct timespec req;
    req.tv_sec = 0;
    req.tv_nsec = 200000000 + rand() % 100000000; // 200ms to 300ms
    nanosleep(&req, NULL);
}

int
uf_file_lock(uf_file_lock_t *lock, enum file_lock_flags flags)
{
    char buf[512];
    char hstname[256];
    pid_t pid;

    if(lock->fd >= 0)
        return UF_COLLECTION_LOCK_OK;

    do {
        int err;
        lock->fd = open(lock->filename,O_CREAT | O_EXCL | O_TRUNC | O_RDWR | O_SYNC, 0664);
        err = errno;
        if ( lock->fd < 0 && err == EEXIST ) {
            if ( flags == FILE_LOCK_NOWAIT ) {
                DPRINTF(LOCK_DEBUG_LEVEL+1, "File (%s) already locked.\n", lock->filename);
                return UF_COLLECTION_LOCK_FAILED;
            }
            DPRINTF(LOCK_DEBUG_LEVEL, "Waiting to obtain lock (filename = %s, desc = '%s')\n", lock->filename, lock->desc);
            wait_for_delete(lock);
            continue;
        } else if ( lock->fd < 0) {
            DPRINTF(LOCK_DEBUG_LEVEL, "Lock (filename = %s, desc = '%s') failed.\n", lock->filename, lock->desc);
            return UF_COLLECTION_LOCK_FAILED;
        }
    } while ( lock-> fd < 0) ;

   	setcloexec(lock->fd);
    gethostname(hstname, 256);
    pid = getpid();
    snprintf(buf, 512,  "%s:%lu\n",hstname, (unsigned long) pid);
    ssize_t ret = write(lock->fd, buf, strlen(buf));
    if ( ret < 0) {
        DPRINTF(LOCK_DEBUG_LEVEL, "Failed to write status information to lock-file (filename = %s, desc = '%s')\n", lock->filename, lock->desc);
    }
    DPRINTF(LOCK_DEBUG_LEVEL, "Lock (filename = %s, desc = '%s') obtained.\n", lock->filename, lock->desc);
    return UF_COLLECTION_LOCK_OK;

}



