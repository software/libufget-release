# - Find sqlite

# This module defines
#  Archive_LIBRARIES, the libraries to link against to use libsqlite3.
#  Archive_FOUND, If false, do not try to use libsqlite3.
#  Archive_INCLUDE_DIR, include directories for libsqlite3.

#=============================================================================
# Copyright 2014, Martin Koehler
#
#
# Distributed under the OSI-approved BSD License (the "License");
# see accompanying file Copyright.txt for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.
#=============================================================================
INCLUDE(CheckFunctionExists) 

if (NOT _incdir)
  if (WIN32)
	  set(_incdir ENV INCLUDE)
  elseif (APPLE)
	  set(_incdir ENV INCLUDE CPATH)
  else ()
	  set(_incdir ENV INCLUDE CPATH)
  endif ()
endif ()

if (NOT _libdir)
  if (WIN32)
    set(_libdir ENV LIB)
  elseif (APPLE)
    set(_libdir ENV DYLD_LIBRARY_PATH)
  else ()
    set(_libdir ENV LD_LIBRARY_PATH)
  endif ()
endif ()

find_path(Archive_INCLUDE_DIR NAMES archive.h
	PATHS
	${_incdir}
	/usr/include
	/usr/local/include
	/opt/local/include	#Macports
  )
set(Archive_NAMES archive libarchive)
find_library(Archive_LIBRARIES NAMES ${Archive_NAMES} PATHS ${_libdir} /usr/lib /usr/lib32 /usr/lib64)

IF(Archive_INCLUDE_DIR AND Archive_LIBRARIES)
	MESSAGE(STATUS "Found Archive header: ${Archive_INCLUDE_DIR}")
	MESSAGE(STATUS "Found Archive library: ${Archive_LIBRARIES}")
	SET(Archive_FOUND TRUE)
ELSE()
	SET(Archive_FOUND FALSE)
ENDIF()

# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Archive DEFAULT_MSG Archive_LIBRARIES Archive_INCLUDE_DIR )
