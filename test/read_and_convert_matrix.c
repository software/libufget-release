/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) Martin Koehler, Jonas Schulze 2015-2023
 */

#include <assert.h>
#include <inttypes.h>
#include "libufget.h"

#if defined(UF_VAL_F32)
typedef float scalar_t;
uf_precision_t const val_type = UF_F32;
#elif defined(UF_VAL_F64)
typedef double scalar_t;
uf_precision_t const val_type = UF_F64;
#endif

#if defined(UF_ROW_I8)
typedef int8_t row_t;
uf_index_t const row_type = UF_I8;
#elif defined(UF_ROW_U8)
typedef uint8_t row_t;
uf_index_t const row_type = UF_U8;
#elif defined(UF_ROW_I16)
typedef int16_t row_t;
uf_index_t const row_type = UF_I16;
#elif defined(UF_ROW_U16)
typedef uint16_t row_t;
uf_index_t const row_type = UF_U16;
#elif defined(UF_ROW_I32)
typedef int32_t row_t;
uf_index_t const row_type = UF_I32;
#elif defined(UF_ROW_U32)
typedef uint32_t row_t;
uf_index_t const row_type = UF_U32;
#elif defined(UF_ROW_I64)
typedef int64_t row_t;
uf_index_t const row_type = UF_I64;
#elif defined(UF_ROW_U64)
typedef uint64_t row_t;
uf_index_t const row_type = UF_U64;
#endif

#if defined(UF_COL_I8)
typedef int8_t col_t;
uf_index_t const col_type = UF_I8;
#elif defined(UF_COL_U8)
typedef uint8_t col_t;
uf_index_t const col_type = UF_U8;
#elif defined(UF_COL_I16)
typedef int16_t col_t;
uf_index_t const col_type = UF_I16;
#elif defined(UF_COL_U16)
typedef uint16_t col_t;
uf_index_t const col_type = UF_U16;
#elif defined(UF_COL_I32)
typedef int32_t col_t;
uf_index_t const col_type = UF_I32;
#elif defined(UF_COL_U32)
typedef uint32_t col_t;
uf_index_t const col_type = UF_U32;
#elif defined(UF_COL_I64)
typedef int64_t col_t;
uf_index_t const col_type = UF_I64;
#elif defined(UF_COL_U64)
typedef uint64_t col_t;
uf_index_t const col_type = UF_U64;
#endif

char const* group_name = "vanHeukelum";
char const* name = "cage3";

// COO storage:
row_t const expected_rows[] = {0, 1, 2, 3, 4, 0, 1, 3, 0, 2, 4, 0, 1, 3, 4, 0, 2, 3, 4};
col_t const expected_cols[] = {0, 0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4};
scalar_t const expected_vals[] = {
        (scalar_t) .666666666666667,
        (scalar_t) .100036889486116,
        (scalar_t) .122185332736106,
        (scalar_t) .050018444743058,
        (scalar_t) .0610926663680531,
        (scalar_t) .366555998208319,
        (scalar_t) .533407112305565,
        (scalar_t) .100036889486116,
        (scalar_t) .300110668458348,
        (scalar_t) .577703998805546,
        (scalar_t) .122185332736106,
        (scalar_t) .366555998208319,
        (scalar_t) .200073778972232,
        (scalar_t) .283314888590275,
        (scalar_t) .150055334229174,
        (scalar_t) .300110668458348,
        (scalar_t) .244370665472212,
        (scalar_t) .183277999104159,
        (scalar_t) .27224066696528
};

// CSR storage:
row_t const expected_rowptr[] = {0, 5, 8, 11, 15, 19};
size_t const perm[] = {0, 5, 8, 11, 15, 1, 6, 12, 2, 9, 16, 3, 7, 13, 17, 4, 10, 14, 18};

int test_coo_rows(row_t const* rows, int64_t nnz) {
    int err = 0;
    for (size_t i = 0; i < nnz; ++i) {
        if (rows[i] != expected_rows[i]) {
            ++err;
            printf("COO row indices do not match: i=%ld expected=%"PRId64" actual=%"PRId64"\n",
                   i, (int64_t) expected_rows[i], (int64_t) rows[i]);
        }
    }
    return err;
}

int test_coo_cols(col_t const* cols, int64_t nnz) {
    int err = 0;
    for (size_t i = 0; i < nnz; ++i) {
        if (cols[i] != expected_cols[i]) {
            ++err;
            printf("COO column indices do not match: i=%ld expected=%"PRId64" actual=%"PRId64"\n",
                   i, (int64_t) expected_cols[i], (int64_t) cols[i]);
        }
    }
    return err;
}

int test_coo_vals(scalar_t const* vals, int64_t nnz) {
    int err = 0;
    for (size_t i = 0; i < nnz; ++i) {
        if (vals[i] != expected_vals[i]) {
            ++err;
            printf("COO values do not match: i=%ld expected=%lf actual=%lf\n",
                   i, (double) expected_vals[i], (double) vals[i]);
        }
    }
    return err;
}

int test_csr_rowptr(row_t const* rowptr, int64_t nrows) {
    int err = 0;
    for (size_t i = 0; i < nrows+1; ++i) {
        if (rowptr[i] != expected_rowptr[i]) {
            ++err;
            printf("CSR rowptr do not match: i=%ld expected=%"PRId64" actual=%"PRId64"\n",
                   i, (int64_t) expected_rowptr[i], (int64_t) rowptr[i]);
        }
    }
    return err;
}

int test_csr_colids(col_t const* colids, int64_t nnz) {
    int err = 0;
    for (size_t i = 0; i < nnz; ++i) {
        if (colids[i] != expected_cols[perm[i]]) {
            ++err;
            printf("CSR colids do not match: i=%ld expected=%"PRId64" actual=%"PRId64"\n",
                   i, (int64_t) expected_cols[perm[i]], (int64_t) colids[i]);
        }
    }
    return err;
}

int test_csr_values(scalar_t const* values, int64_t nnz) {
    int err = 0;
    for (size_t i = 0; i < nnz; ++i) {
        if (values[i] != expected_vals[perm[i]]) {
            ++err;
            printf("CSR values do not match: i=%ld expected=%lf actual=%lf\n",
                   i, (double) expected_vals[perm[i]], (double) values[i]);
        }
    }
    return err;
}

extern int uf_get_quicksort_thresh;

int main(void) {
    printf("sizeof(scalar_t) = %ld\n", sizeof(scalar_t));
    printf("sizeof(row_t) = %ld\n", sizeof(row_t));
    printf("sizeof(col_t) = %ld\n", sizeof(col_t));

    uf_collection_t* col = uf_collection_init();
    uf_matrix_t* mat = uf_collection_get_by_name(col, group_name, name);

    int err;
    uf_field_t field;
    int64_t nrows, ncols, nnz;
    row_t* rows;
    col_t* cols;
    scalar_t* vals;

    err = uf_matrix_coord_extended(
            &field
            , &nrows, &ncols, &nnz
            , (void**) &rows, row_type
            , (void**) &cols, col_type
            , (void**) &vals, val_type
            , mat);
    assert(!err && "matrix load must succeed");
    assert(field == UF_REAL);
    assert(nrows == 5);
    assert(ncols == 5);
    assert(nnz == 19);
    assert(rows);
    assert(cols);
    assert(vals);

    err += test_coo_rows(rows, nnz);
    err += test_coo_cols(cols, nnz);
    err += test_coo_vals(vals, nnz);
    assert(!err && "COO data must be correct");

    const char* env;
    if ((env = getenv("UFTEST_MEMSAFE"))) {
        int val = atoi(env);
        printf("Calling uf_matrix_conv_memsave(%d)\n", val);
        uf_matrix_conv_memsave(val);
    }
    if ((env = getenv("UFTEST_QUICKSORT_THRESH"))) {
        int val = atoi(env);
        printf("Setting uf_get_quicksort_thresh=%d\n", val);
        uf_get_quicksort_thresh = val;
    }

    err = uf_matrix_coord_to_csr_extended(
            field
            , nrows, ncols, nnz
            , (void**) &rows, row_type
            , (void**) &cols, col_type
            , (void**) &vals, val_type
    );
    assert(!err && "conversion to CSR must succeed");

    err += test_csr_rowptr(rows, nrows);
    err += test_csr_colids(cols, nnz);
    err += test_csr_values(vals, nnz);
    assert(!err && "CSR data must be correct");

    free(rows);
    free(cols);
    free(vals);
    uf_matrix_free(mat);
    uf_collection_finalize(col);

    return err;
}
