#!/bin/bash

set -e

TMPDIR=${TMPDIR:-/tmp}
UF_COLLECTION_CACHE_DIR=$(mktemp -d ${TMPDIR%/}/uftest.XXXXXXXXXX)

echo "=== Check that matrix isn't present, yet i.e. trying to download it fails"
UF_COLLECTION_BASEURL=boom bash -c './src/ufget-update update; test $? -ne 0'

echo '=== Download matrix'
./src/ufget-update download HB 1138_bus

echo '=== Ensure that the matrix is not being downloaded again'
UF_COLLECTION_BASEURL=boom ./src/ufget-update download HB 1138_bus

echo '=== ... not even after updating the database'
./src/ufget-update update
UF_COLLECTION_BASEURL=boom ./src/ufget-update download HB 1138_bus

echo '=== ... or after moving the cache'
mv $UF_COLLECTION_CACHE_DIR $UF_COLLECTION_CACHE_DIR.moved
UF_COLLECTION_CACHE_DIR=$UF_COLLECTION_CACHE_DIR.moved
UF_COLLECTION_BASEURL=boom ./src/ufget-update download HB 1138_bus

echo '=== Cleanup'
rm -rf $UF_COLLECTION_CACHE_DIR
