#!/bin/bash

set -e

export UF_COLLECTION_CACHE_DIR=$(mktemp --tmpdir -d uftest.XXXXXXXXXX)
UF_GLOBAL_LOCK=$UF_COLLECTION_CACHE_DIR/lock
UF_MATRIX_LOCK=$UF_COLLECTION_CACHE_DIR/MM/HB/1138_bus.lock

echo '=== Create some dangling and corrupted lock files'
mkdir -p $(dirname $UF_MATRIX_LOCK)
echo boom > $UF_GLOBAL_LOCK
echo boom > $UF_MATRIX_LOCK
echo $UF_GLOBAL_LOCK
echo $UF_MATRIX_LOCK

echo '=== Run ufget-update unlock'
./src/ufget-update unlock

echo '=== Ensure that the global lock file has been removed'
test ! -f $UF_GLOBAL_LOCK

echo '=== ... as well as the matrix lock file'
test ! -f $UF_MATRIX_LOCK

echo '=== Cleanup'
rm -rf $UF_COLLECTION_CACHE_DIR
