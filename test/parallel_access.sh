#!/bin/bash
#SBATCH -J uftest
#SBATCH -n10
#SBATCH -N10

set -eu

TMPDIR=${TMPDIR:-/tmp}
export UF_COLLECTION_CACHE_DIR=$(mktemp -d ${TMPDIR%/}/uftest.XXXXXXXXXX)
export UF_COLLECTION_VERBOSE=10

if [ ${SLURM_JOB_ID+x} ]
then
	echo '=== Running inside Slurm environment'
	module load compiler/gcc/10 libraries/ufget/1.1.1
	run() { srun $@; }
else
	echo '=== Running locally'
	run() {
		for _ in $(seq 10)
		do
			$@ &
			UF_PIDS+="$! "
		done
		for pid in $UF_PIDS
		do
			wait $pid || {
				echo "process $pid failed with exit code $?"
				UF_FAILED=1
			}
		done
		[ ! ${UF_FAILED-} ]
	}
fi

env | grep UF_COLLECTION

UF_COMMAND='./src/ufget-update download HB 1138_bus'

echo '=== Test Case 1: matrix has been downloaded already'
$UF_COMMAND
run $UF_COMMAND

echo '=== Test Case 2: matrix has *not* been downloaded yet'
rm -rf $UF_COLLECTION_CACHE_DIR/*
run $UF_COMMAND

echo '=== Cleanup'
rm -rf $UF_COLLECTION_CACHE_DIR

