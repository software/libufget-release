Changelog
=========

Version 1.3.1 - May 5, 2023
---------------------------
* Fixes in the locking procedure

Version 1.3.0 - Apr 18, 2023
----------------------------
* Add `uf_matrix_coord_extended` which allows reading matrix entries using
  single precision floating point numbers
* Add `uf_matrix_coord_to_csr_extended` which can handle different matrix
  scalar and index types
* Add `uf_collection_revision` to obtain the revision of the database
* Add CMake option `LIBUFGET_BUILD_TESTING` (default: off).
* Add const qualifier where useful
* Fix relocation of the cache dir
* Improved `UF_COLLECTION_VERBOSE` handling
* The cache directories inherits the permission from its parent directory. This
  allows to share the cache between different users of a system, e.g. in a
  network share or /tmp
* The database is now locked during download/update.
* The matrices are locked during download.
* Database optimizations, `DB_VERSION` is now 4
* ufget-update subcommands `unlock` and `clean` added.
* Exported symbols are now versioned.


Version 1.2.1 - Oct 18, 2022
----------------------------
* Fix missing include when used from C++

Version 1.2.0 - Oct 11, 2022
----------------------------
* Add `uf_query_sql_va` to be able to deal with varying arguments
* Add `uf_matrix_copy`
* Add `uf_matrix_dup`
* Fix: Bug if CURL is not in standard path
* Add CMake alias target `libufget::ufget`; read INSTALL.md for
  how to add libufget as a CMake sub-project
* Rename CMake option `EXAMPLES` to `LIBUFGET_BUILD_EXAMPLES`
  and switch it off by default

Version 1.1.1 - Sep 7, 2022
---------------------------
* Bugfixes in CMAKE building system

Version 1.1.0 - Sep 5, 2022
----------------------------
* Spell checking of the source code
* Restyle CMake to a more modern way
* Install cmake target for FIND_PACKAGE config mode
* Only symbols with LIBUFGET_EXPORT in libufget.h are exported
* Add `uf_matrix_fprint`
* Add `uf_query_count`

Version 1.0.3 - Jun 15th, 2020
------------------------------
* Fix URL. Collection moved to http://sparse-files.engr.tamu.edu
* Fix compiler warnings with gcc 9.x

Version 1.0.2 - Sep 20th, 2017
------------------------------
* Fix URL. Collection moved to https://sparse.tamu.edu

Version 1.0.1 - Jun 21st, 2017
------------------------------
* Fix URL. The UFcollection now uses https only
* Enable FOLLOWLOCATION for libcurl
* Fix NAN with xmin and xmax field.
* Database version set to 2

Version 1.0.0 - Jun 20th, 2017
------------------------------
* Initial release.


