/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) Martin Koehler, Jonas Schulze, 2023
 */

#include <assert.h>
#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "libufget.h"

int frobenius32(uf_matrix_t const* mat, double* nrm2)
{
    // Prepare variables to hold matrix data:
    uf_field_t field;
    int64_t rows, cols, nnz;
    int32_t *rowptr, *colptr;
    float *values;

    // Download and read matrix from disk:
    int err = uf_matrix_coord_extended(
            &field, &rows, &cols, &nnz,
            (void **) &rowptr, UF_I32,
            (void **) &colptr, UF_I32,
            (void **) &values, UF_F32,
            mat);
    if (err) {
        fprintf(stderr, "Could not read matrix as float: %s/%s\n", mat->group_name, mat->name);
        return 1;
    }

    // Compute squared Frobenius norm
    double fnorm = 0;
    for (size_t i = 0; i < mat->nnz; ++i) {
        fnorm += values[i] * values[i];
    }
    *nrm2 = fnorm;

    free(rowptr);
    free(colptr);
    free(values);

    return 0;
}

int frobenius64(uf_matrix_t const* mat, double* nrm2)
{
    // Prepare variables to hold matrix data:
    uf_field_t field;
    int64_t rows, cols, nnz;
    int32_t *rowptr, *colptr;
    double *values;

    // Download and read matrix from disk:
    int err = uf_matrix_coord_extended(
            &field, &rows, &cols, &nnz,
            (void **) &rowptr, UF_I64,
            (void **) &colptr, UF_I32,
            (void **) &values, UF_F64,
            mat);
    if (err) {
        fprintf(stderr, "Could not read matrix as double: %s/%s\n", mat->group_name, mat->name);
        return 1;
    }

    // Compute squared Frobenius norm
    double fnorm = 0;
    for (size_t i = 0; i < mat->nnz; ++i) {
        fnorm += values[i] * values[i];
    }
    *nrm2 = fnorm;

    free(rowptr);
    free(colptr);
    free(values);

    return 0;
}

int main(int argc, char **argv)
{
    uf_collection_t *col = uf_collection_init();
    uf_matrix_t *mat = uf_collection_get_by_name(col, "HB", "1138_bus");
    assert(mat != NULL);

    int err;
    double f32, f64;
    err = frobenius32(mat, &f32);
    assert(!err);
    err = frobenius64(mat, &f64);
    assert(!err);

    uf_matrix_free(mat);
    uf_collection_finalize(col);

    printf("Frobenius nrm2 from float:  %lg\n", f32);
    printf("Frobenius nrm2 from double: %lg\n", f64);

    double delta = fabs(f64 - f32) / f64;
    printf("Relative error: %lg * FLT_EPSILON\n", delta / FLT_EPSILON);

    return delta > FLT_EPSILON;
}
