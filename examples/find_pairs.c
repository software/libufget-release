/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) Martin Koehler, 2015-2022
 * Copyright (C) Martin Koehler, Jonas Schulze, 2023
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>

#include "libufget.h"

int main(int argc, char **argv)
{
    uf_collection_t *col;
    uf_matrix_t *mat = NULL;
    uf_matrix_t *mat2 = NULL;
    uf_query_t *query_outer;
    uf_query_t *query_inner;

    printf("libufget - Find Pairs demo\n");
    printf("===========================\n");



    /* Init Collection  */

    /* Default Init  */
    col = uf_collection_init();

    /* Init with flags */
    // col = uf_collection_init1(UF_COLLECTION_VERBOSE|UF_COLLECTION_CLEANUP);

    printf("Database contains %d matrices.\n", uf_collection_num_matrices(col));

    /* SQL Query */
    query_outer = uf_query_sql(col, "nrows = ncols AND isBinary != 1");

    while ( (mat = uf_query_next(query_outer)) != NULL ) {
        /* Load the matrix  */
        printf("Outer Matrix... %s\n", mat->name);

        query_inner = uf_query_sql_va(col, "nrows = %d AND id != %d", mat->rows, mat->id);

        while ( (mat2 = uf_query_next(query_inner)) != NULL ) {
            printf(" -- %s\n", mat2->name);

            uf_matrix_free(mat2);
        }

        uf_query_free(query_inner);
        /* Download marix and skip if not possible.  */
        /* if ( uf_matrix_get(mat) != 0 ){ */
        /*     printf("Cannot download Matrix (%s/%s) skipping.\n", mat->group_name, mat->name); */
        /*     uf_matrix_free(mat);  */
        /*     continue;  */
        /* } */
       uf_matrix_free(mat);
    }
    // uf_collection_cleanup(col);
    /* Clean up  */
    uf_query_free(query_outer);
    uf_collection_finalize(col);

    return 0;
}
